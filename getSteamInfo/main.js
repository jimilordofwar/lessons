var xhReq = new XMLHttpRequest();
xhReq.open("GET", './games.json', false);
xhReq.send(null);
const games = JSON.parse(xhReq.responseText);
const gamesContainer = document.querySelector('#gamesContainer');
/*
games.applist.apps.forEach((game) => {
	console.log(game);
});
*/

const searchGameInput = document.querySelector('#searchInput');

function findGame(gameName) {
	if (gameName == '') return false;

	let counter = 0;
	let foundGames = [];

	games.applist.apps.forEach((game, index) => {
		let newName = game.name.replace("-", " ");
		var regex = new RegExp(gameName, 'gmi');
		if (newName.match(regex)) {
			foundGames[index] = game;
		}
	});

	let content = "";
	foundGames.forEach((game) => {
		var xhReq = new XMLHttpRequest();
		xhReq.open("GET", 'http://localhost:3000/'+game.appid, false);
		xhReq.send(null);
		const foundGame = JSON.parse(xhReq.responseText);

		if (! foundGame[game.appid]['success']) return false;

		if (foundGame[game.appid].success === true) {
			let gameData = foundGame[game.appid].data;
			console.log("GAME TYPE: ", gameData.type)
			if (gameData.type == 'game') {
				console.log("FOUND GAME:", gameData);
				content += "<div class='game'>";
				content += "<h1>" + gameData['name']+"</h1>";
				content += "<p>" + gameData['short_description'] + "</p>";
				content += "<ul>";
				content += "<li>Release Date: "+gameData['release_date']['date']+"</li>";
				if (gameData['price_overview']) {
					content += "<li>Price: " + gameData['price_overview']['final_formatted']+"</li>";
				}
				content += "</ul>";
				content += "<img style='width:100%' src='"+gameData['screenshots'][0]['path_full']+"'>";
				content += "</div>";
			}
		}
	});
	gamesContainer.innerHTML = content;

	return false;
}


document.querySelector('#gamesSearch').addEventListener('submit', (e) => {
	e.preventDefault();
	findGame(searchGameInput.value);
})

