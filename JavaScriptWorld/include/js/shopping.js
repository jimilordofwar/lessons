let addItemInput = document.querySelector('#new-item');
let addItemButton = document.querySelector('#add-item');
let shoppingList = document.querySelector('#shopping');
let addItemForm = document.querySelector('#add-item-form');
let existingItems = {items: []};
let itemJson;

// check local storage
let shoppingListStorage = window.localStorage.getItem("shopping-list");

// if no storage then set it
if (! shoppingListStorage) {
	window.localStorage.setItem('shopping-list', JSON.stringify(existingItems));
}
else {
	shoppingList.innerHTML = "";
	itemJson = JSON.parse(shoppingListStorage);
	existingItems = itemJson;
	generateList();
}

addItemForm.addEventListener('submit', function(event) {
	let inputText = addItemInput.value;
	shoppingList.innerHTML = ""
	existingItems.items.push(inputText);

	console.log(existingItems);

	generateList();

	window.localStorage.setItem('shopping-list', JSON.stringify(existingItems));

	console.log(existingItems.toString());

	addItemInput.value = "";
	addItemInput.focus();
	event.preventDefault();
	return;
});

function deleteItem(id) {
	//existingItems.items[id].pop();
	existingItems.items = existingItems.items.filter((item, index) => {
		console.log("ID IS:" +id, "INDEX IS: "+index);
		return index !== id;
	});

	generateList();
	console.log(existingItems);
}

function generateList() {
	shoppingList.innerHTML = "";
	itemJson.items.forEach((item, index) => {
		shoppingList.innerHTML += "<li class='list-group-item d-flex justify-content-between align-items-center list-group-item-warning'>"
			+ item
			+ '<button type="button" class="btn btn-danger border shadow-sm" onclick="deleteItem(' + index + ')">x</span>'
			+ "</li>";
	});

	window.localStorage.setItem('shopping-list', JSON.stringify(existingItems));
}